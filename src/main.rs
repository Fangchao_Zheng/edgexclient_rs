use chrono::Duration;
use std::sync::mpsc::{self, Receiver, Sender};
use std::thread::{self, JoinHandle};

mod app_timer;
mod async_mqtt_client;
mod command_handler;
mod data_server;

use data_server::DataServerFactory;

use log::{error, info, warn};

#[derive(Debug, Clone, Copy)]
enum AppCommand {
    Run,
    Pause,
    Quit,
}
use AppCommand::*;

#[derive(Clone)]
enum AppMessage {
    Cmd(AppCommand),
    TimeTrigger,
    MqttMessage(paho_mqtt::Message),
    MqttDisconnect,
}
use AppMessage::*;

fn main() {
    simple_logger::SimpleLogger::new()
        .with_level(log::LevelFilter::Info)
        .with_threads(false)
        .init().unwrap();

    let (tx, rx) = mpsc::channel::<AppMessage>();

    let app_handle = app_thread_init(tx.clone(), rx, Duration::seconds(5));
    input_thread_init(tx.clone());

    app_handle.join().unwrap();
}

fn input_thread_init(tx: Sender<AppMessage>) -> JoinHandle<()> {
    let f = move || {
        let input = getch::Getch::new();
        loop {
            let ch = input.getch().unwrap();

            //Generate and send the message
            let msg = match ch.into() {
                'q' => Some(Cmd(Quit)),
                'r' => Some(Cmd(Run)),
                'p' => Some(Cmd(Pause)),
                _ => None,
            };
            if let Some(v) = msg {
                tx.send(v).unwrap();
            }

            //Process the Input Thread Quit Event
            if let 'q' = ch.into() {
                break;
            }
        }
    };

    thread::spawn(f)
}

fn app_thread_init(
    tx: Sender<AppMessage>,
    rx: Receiver<AppMessage>,
    duration: Duration,
) -> JoinHandle<()> {
    //Data Server
    let data_server = DataServerFactory::create("RamdomDataServer").unwrap();

    //Mqtt Client
    let mqtt_client = async_mqtt_client::AsyncMqttClient::new(tx.clone())
        .set_message_callback(AppMessage::MqttMessage)
        .set_disconnect_callback(AppMessage::MqttDisconnect)
        .init()
        .unwrap();

    //App Timer
    let mut timer = app_timer::AppTimer::new(tx.clone());

    //Command Handler
    let mut command_handler = command_handler::CommandHandler::new().unwrap();

    let f = move || {
        for msg in rx {
            match msg {
                AppMessage::Cmd(cmd) => {
                    info!("Receive Cmd : {:?}", cmd);

                    match cmd {
                        AppCommand::Run => timer
                            .start(duration, AppMessage::TimeTrigger)
                            .unwrap_or_else(|e| {
                                info!("Timer start error : {}", e);
                            }),
                        AppCommand::Pause => timer.cancel().unwrap_or_else(|e| {
                            info!("Timer cancel error : {}", e);
                        }),
                        AppCommand::Quit => {
                            // The timer will stop after drop, so do nothing with "Quit"
                            break;
                        }
                    }
                }
                AppMessage::TimeTrigger => {
                    info!("TimeTrigger");
                    let content = data_server.get_data();
                    mqtt_client.publish("DataTopic", content).unwrap();
                }
                AppMessage::MqttMessage(msg) => {
                    info!(
                        "Receive Mqtt Message : {} - {}",
                        msg.topic(),
                        msg.payload_str()
                    );
                    let response = command_handler.handle_message(msg.payload_str());

                    match response {
                        Ok(v) => {
                            mqtt_client.publish("ResponseTopic", v).unwrap();
                        }
                        Err(e) => {
                            error!("Failed to handler message : {}", e);
                        }
                    };
                } //AppMessage::MqttMessage

                AppMessage::MqttDisconnect => {
                    warn!("Mqtt Connect Lost");
                    break;
                }
            } //match msg
        }
    };
    thread::spawn(f)
}
