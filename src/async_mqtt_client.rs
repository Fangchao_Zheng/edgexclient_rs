use paho_mqtt as mqtt;

use std::error::Error;
use std::sync::mpsc::Sender;
use std::time::Duration;

use log::info;

const BROKER_URL: &str = "tcp://192.168.1.103:1883";
const CLIENT_NAME: &str = "EXMqttClient";

// Define the qos.
const QOS_LEVEL_1: i32 = 1;

pub struct AsyncMqttClient<T>
where
    T: Send + Clone,
{
    client: mqtt::AsyncClient,
    tx: Sender<T>,
}

impl<T> AsyncMqttClient<T>
where
    T: Send + Clone + 'static,
{
    #[must_use]
    pub fn new(tx: Sender<T>) -> Self {
        // Define the set of options for the Client.
        let cli = mqtt::CreateOptionsBuilder::new()
            .server_uri(BROKER_URL)
            .client_id(CLIENT_NAME)
            .create_client()
            .unwrap();

        AsyncMqttClient {
            client: cli,
            tx,
        }
    }

    pub fn publish<S, V>(&self, topic: S, content: V) -> Result<(), Box<dyn Error>>
    where
        S: Into<String>,
        V: Into<Vec<u8>>,
    {
        let msg = mqtt::Message::new(topic, content, QOS_LEVEL_1);
        self.client.publish(msg).wait()?;
        return Ok(());
    }

    #[must_use]
    pub fn set_message_callback<S>(self, msg: S) -> Self
    where
        S: Fn(mqtt::Message) -> T + 'static,
    {
        let tx = self.tx.clone();
        self.client.set_message_callback(move |_cli, data| {
            if let Some(v) = data {
                tx.send(msg(v)).expect("Failed to Send Mqtt Data");
            }
        });
        self
    }

    #[must_use]
    pub fn set_disconnect_callback(self, msg: T) -> Self {
        let tx = self.tx.clone();
        self.client.set_connection_lost_callback(move |_cli| {
            tx.send(msg.clone())
                .expect("Failed to Send Connect Lost Message");
        });
        self
    }

    pub fn init(self) -> Result<Self, Box<dyn Error>> {
        self.client
            .set_connected_callback(|_cli: &mqtt::AsyncClient| {
                info!("Connected.");
            });

        // Define the set of options for the connection.
        let conn_opts = mqtt::ConnectOptionsBuilder::new()
            .keep_alive_interval(Duration::from_secs(20))
            .clean_session(true)
            .finalize();

        self.client.connect(conn_opts).wait()?;

        self.client.subscribe("CommandTopic", QOS_LEVEL_1).wait()?;

        Ok(self)
    }
}

impl<T> Drop for AsyncMqttClient<T>
where
    T: Send + Clone,
{
    fn drop(&mut self) {
        if self.client.is_connected() {
            self.client.disconnect(None).wait().unwrap();
        }
    }
}
