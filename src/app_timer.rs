pub use timer::{Guard, MessageTimer};

pub use chrono::Duration;

use std::sync::mpsc::Sender;

pub struct AppTimer<T>
where
    T: 'static + Send + Clone,
{
    timer: MessageTimer<T>,
    guard: Option<timer::Guard>,
}

impl<T> AppTimer<T>
where
    T: 'static + Send + Clone,
{
    pub fn new(tx: Sender<T>) -> Self {
        let timer = MessageTimer::new(tx);
        AppTimer { timer, guard: None }
    }
    pub fn start(&mut self, dt: Duration, msg: T) -> Result<(), &str> {
        if let None = self.guard {
            let gd = self.timer.schedule_repeating(dt, msg);
            self.guard = Some(gd);
            Ok(())
        } else {
            Err("Timer is Already Running")
        }
    }

    pub fn cancel(&mut self) -> Result<(), &str> {
        if let Some(_) = self.guard {
            //Drop the Guard
            self.guard.take();
            Ok(())
        } else {
            Err("Timer is Not Running")
        }
    }
}
