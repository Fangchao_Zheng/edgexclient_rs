use json::object;

pub trait DataServer {
    fn get_data(&self) -> String;
}

struct RamdomDataServer;

impl DataServer for RamdomDataServer {
    fn get_data(&self) -> String {
        let value: u8 = rand::random();

        object! {name : "MyDevice",
        cmd:"ping",
        //method:"get",
        ping: value}
        .dump()
    }
}

pub struct DataServerFactory;

impl DataServerFactory {
    pub fn create<T>(name: T) -> Result<Box<dyn DataServer + Send>, &'static str>
    where
        T: Into<String>,
    {
        match name.into().as_str() {
            "RamdomDataServer" => Ok(Box::new(RamdomDataServer {})),
            _ => Err("Unknow Data Server Name"),
        }
    }
}
