use std::error::Error;

use log::info;

const TIMESTAMP_FORMAT: &'static str = "%F %H:%M:%S:%3f";

pub struct CommandHandler {
    msg: String,
}

impl CommandHandler {
    pub fn new() -> Result<Self, &'static str> {
        Ok(CommandHandler { msg: String::from("Init Message") })
    }

    pub fn handle_message<T>(&mut self, msg: T) -> Result<String, Box<dyn Error>>
    where
        T: Into<String>,
    {
        //Parse Json Data
        let data = json::parse(&msg.into())?;

        let cmd = data["cmd"].as_str().ok_or("Unknow cmd")?;
        let method = data["method"].as_str().ok_or("Unknow method")?;
        let uuid = data["uuid"].as_str().ok_or("Unknow uuid")?;

        match method {
            "get" => {
                let ret = self.process_get(cmd, uuid)?;
                Ok(ret)
            }
            "set" => {
                let cmd_data = data[cmd].as_str().ok_or("Unknow cmd data")?;
                let ret = self.process_set(cmd, cmd_data, uuid)?;
                Ok(ret)
            }
            _ => Err(format!("No such method : {}", method).into()),
        }
    }

    fn process_set(&mut self, cmd: &str, cmd_data: &str, uuid: &str) -> Result<String, String> {
        match cmd {
            "message" => {
                self.msg = cmd_data.to_string();
                Ok(())
            }
            "ex_cmd" => {
                info!("Excute cmd : {cmd_data}");
                Ok(())
            }
            _ => Err(format!("No such cmd can be set : {cmd}")),
        }?;

        Ok(json::object! {
            "cmd" : cmd,
            //"method": "set",
            "uuid" : uuid,
        }
        .dump())
    }

    fn process_get(&mut self, cmd: &str, uuid: &str) -> Result<String, String> {
        let data = match cmd {
            "randnum" => {
                let value = rand::random::<u8>().to_string();
                Ok(value)
            }

            "ping" => Ok("Awake".to_string()),

            "message" => Ok(self.msg.clone()),

            "timestamp" => Ok(chrono::Local::now().format(TIMESTAMP_FORMAT).to_string()),

            _ => Err(format!("No such cmd can be get : {cmd}")),
        }?;

        let mut obj = json::object! {
            "cmd" : cmd,
            //"method": "get",
            "uuid" : uuid,
        };

        obj.insert(cmd, data.as_str())
            .map_err(|e| format!("Failed to create JSON Value : {e}"))?;

        Ok(obj.dump())
    }
}
